#WWU Fontello#

##How to Edit##
On the command line, go to the repository folder and execute the openfont.sh file.
This will open up a web browser where you can edit the font to your liking.

When finished editing, download and unzip the updated font.  Copy and replace the contents over the existing contents of this repo.

##List of Projects Used In##

1. wp-wwugenesis (Wordpress Theme for Western)
2. wwuzen (Main Drupal Theme)
